import React from 'react';
import { View } from 'react-native';
import MapInput from '../components/mapinput';
import MapView from '../components/mapview';
import { getLocation, geocodeLocationByName } from '../services/location-service';
import { Button } from 'react-native';

class MapContainer extends React.Component {
    state = {
        region: {}
    };

    componentDidMount() {
        this.getInitialState();
    }

    getInitialState() {
        getLocation().then(
            (data) => {
                console.log(data);
                this.setState({
                    region: {
                        latitude: data.latitude,
                        longitude: data.longitude,
                        latitudeDelta: 0.003,
                        longitudeDelta: 0.003
                    }
                });
            }
        );
    }

    getCoordsFromName(loc) {
        this.setState({
            region: {
                latitude: loc.lat,
                longitude: loc.lng,
                latitudeDelta: 0.003,
                longitudeDelta: 0.003
            }
        });
    }

    onMapRegionChange(region) {
        this.setState({ region });
    }

    render() {
        //console.log('state:')
        //console.log(this.state)
        const { navigation } = this.props
        return (
            <View style={{ flex: 1 }}>
                <View style={{ position: 'absolute', width: '100%', zIndex: 99 }}>
                    <MapInput notifyChange={(loc) => this.getCoordsFromName(loc)}
                    />
                </View>

                {this.state.region['latitude'] ?
                    <View style={{ flex: 1 }}>
                        <MapView
                            region={this.state.region}
                            onRegionChange={(reg) => this.onMapRegionChange(reg)} />
                        <Button title='Choose this location' onPress={() => { navigation.navigate('NewLocation', { latitude: this.state.region.latitude, longitude: this.state.region.longitude }) }} />
                    </View> : null}
                    
            </View>
        );
    }
}

export default MapContainer;